<?php

namespace Drupal\webpurify\Plugin\WebformHandler;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\filter\FilterProcessResult;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformInterface;
use Drupal\webform\WebformSubmissionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Webform example handler.
 *
 * @WebformHandler(
 *   id = "webpurify",
 *   label = @Translation("Webpurify"),
 *   category = @Translation("Webpurify"),
 *   description = @Translation("Webform handler to utilize the Webpurify API."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_IGNORED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_REQUIRED,
 * )
 */
class WebpurifyWebformHandler extends WebformHandlerBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'method' => 'count',
      'message' => $this->t('The content in this submission was found to be inappropriate and has been blocked.'),
      'mode' => 'types',
      'types' => ['textfield','textarea'],
      'elements' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    // Message.
    // Message.

    $form['webpurify'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Webpurify settings'),
    ];

    $form['webpurify']['method'] = [
      '#title' => $this->t('Purify method'),
      '#description' => $this->t('The method to use on submitted content. Replace will replace any text that is triggered detected by the WebPurify filter with the character selected in the WebPurify module settings.'),
      '#type' => 'radios',
      '#options' => [
        // The "Block" method is really just getting a count of hits in the filter.
        'count' => $this->t('Block'),
        'replace' => $this->t('Replace'),
      ],
      '#required' => TRUE,
      '#default_value' => $this->configuration['method'] ?? $this->defaultConfiguration()['method'],
    ];

    $form['webpurify']['message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Message to be displayed when a form submission fails validation. This will not be shown when using the "Replace" method.'),
      '#default_value' => $this->configuration['message'] ?? $this->defaultConfiguration()['message'],
      '#required' => TRUE,
    ];

    // TODO: Should we make it configurable, what element types to apply filtering to?
    // Or, apply to only specific elements?

    // Get a list of all element types that appear  in the form.
    // Get a list of all elements that apply to theform.


    $form['webpurify']['mode'] = [
      '#title' => $this->t('Applies to'),
      '#description' => $this->t('Select the mode to determine which elements to purify. "Elements types" will apply the Webpurify filter to all elements in the form of the selected types. "Specific elements" allows you to only filter on the individual elements selected from the form.'),
      '#type' => 'select',
      '#options' => [
//        'none' => $this->t('Choose one'),
        'types' => $this->t('Element types'),
        // The "Block" method is really just getting a count of hits in the filter.
        'elements' => $this->t('Specific elements'),
      ],
      '#default_value' => $this->configuration['mode'] ?? $this->defaultConfiguration()['mode'],
      '#required' => TRUE,
    ];


    $elements = $this->getWebform()->getElementsDecoded();
    $availableTypeOptions = [];
    $availableElementOptions = [];
    // Don't apply to elements that don't make sense. i.e. values that don't provide the user unstructured input.
    $excludedTypes = ['webform_markup', 'captcha', 'hidden', 'select', 'radios', 'checkboxes'];
    foreach ($elements as $key => $element) {
      if (!in_array($element['#type'], $excludedTypes)) {
        $availableTypeOptions[$element['#type']] = $element['#type'];
        $availableElementOptions[$key] = $element['#title'] ?? $key;
      }
    }


    $form['webpurify']['types'] = [
      '#title' => $this->t('Element types'),
      '#description' => $this->t('You must select "Apply to any element of the selected types" for this to have an effect.'),
      '#type' => 'select',
      '#options' => $availableTypeOptions,
      '#multiple' => TRUE,
      '#default_value' => $this->configuration['types'] ?? $this->defaultConfiguration()['types'],
    ];

    $form['webpurify']['elements'] = [
      '#title' => $this->t('Specific elements'),
      '#description' => $this->t('Select what elements to use Webpurify on. You must select "Apply only to the specific elements selected" for this to have any effect.'),
      '#type' => 'select',
      '#options' => $availableElementOptions,
      '#multiple' => TRUE,
      '#default_value' => $this->configuration['elements'] ?? $this->defaultConfiguration()['elements'],
    ];


    return $this->setSettingsParents($form);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['method'] = $form_state->getValue('method');
    $this->configuration['message'] = $form_state->getValue('message');
    $this->configuration['mode'] = $form_state->getValue('mode');
    $this->configuration['types'] = $form_state->getValue('types');
    $this->configuration['elements'] = $form_state->getValue('elements');
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $formState, WebformSubmissionInterface $webformSubmission) {
    $config = $this->getConfiguration();
    $settings = $config['settings'];
    $webPurify = \Drupal::service('webpurify.api');
    foreach ($webformSubmission->getData() as $key => $value) {
      // Only apply filter to selected elements.
      if ($this->appliesToElement($key)) {
        if ($settings['method'] == 'count') {
          // make the call to the WebPurify API.
          $count = (bool) $webPurify->count($value);
          if ($count) {
            $formState->setErrorByName($key, $this->t($settings['message']));
          }
        }
        // If the method is "replace" instead, then the value must be changed in
        // a the preCreate method.
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(WebformSubmissionInterface $webformSubmission){
    // If applicable, replace the vales of a submitted webform with a filtered version.
    $config = $this->getConfiguration();
    $settings = $config['settings'];
    $webPurify = \Drupal::service('webpurify.api');
    // Only apply filter to selected elements.
    $values = $webformSubmission->getData();
    foreach ($values as $key => $value) {
      if ($this->appliesToElement($key)) {
        if ($settings['method'] == 'replace') {
          // make the call to the WebPurify API.
          $replace = $webPurify->replace($value);
          if ($replace) {
            $values[$key] = $replace;
            $webformSubmission->setElementData($key, $replace);
          }
        }
        // If the method is "replace" instead, then the value must be changed in
        // a the preCreate method.
      }
    }
  }


  /**
   * Helper to dermine whether webpurify should be applied to a particular field
   * based on the handler configuration.
   * @param $elementKey
   * @return bool
   */
  public function appliesToElement($elementKey){
    $config = $this->getConfiguration();
    $settings = $config['settings'];
    $mode = $settings['mode'];
    $elements = $this->getWebform()->getElementsDecoded();
    if ($mode == 'types') {
      $types = $settings['types'];
      foreach ($types as $type) {
        if (isset($elements[$elementKey]['#type']) && $elements[$elementKey]['#type'] == $type) {
          return TRUE;
        }
      }
    }
    else if ($mode == 'elements') {
      $applicableElements = $settings['elements'];
      foreach ($applicableElements as $applicableElement) {
        if ($elementKey == $applicableElement) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }
}
